package com.exercise;

import com.exercise.rooms.RoomAnalyzer;
import com.exercise.rooms.WallpaperCalculator;
import com.exercise.rooms.converter.RoomConverter;
import com.exercise.rooms.model.Room;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class AnalyzeRoomsApp {

    public static void main(String[] args) {

        if (args.length < 2) {
            System.out.println("Invalid number of arguments.");
            printHelp();
            return;
        }

        String command = args[0];
        String filepath = args[1];

        try (Stream<String> stream = Files.lines(Paths.get(filepath))) {

            RoomConverter converter = new RoomConverter();
            Stream<Room> rooms = converter.toRooms(stream);

            switch (command) {
                case "-t":
                    int total = new WallpaperCalculator().total(rooms);
                    System.out.println("Wallpaper to command: " + total);
                    break;
                case "-c":
                    System.out.println("Cubic rooms:");
                    new RoomAnalyzer()
                            .filterCubic(rooms, new WallpaperCalculator().byWallpaperNeedComparator())
                            .forEach(room ->
                                System.out.println(converter.fromRoom(room))
                            );
                    break;
                case "-d":
                    System.out.println("Duplicate rooms:");
                    new RoomAnalyzer()
                            .filterDuplicate(rooms)
                            .forEach(room ->
                                System.out.println(converter.fromRoom(room))
                            );
                    break;
                default:
                    System.out.println("Command '" + command + "' is unknown.");
                    printHelp();
                    return;
            }

        } catch (IOException e) {
            System.out.println("File " + filepath + " has not been found.");
            printHelp();
        }

    }

    static void printHelp() {
        System.out.println("Following 2 arguments are mandatory:");
        System.out.println(" 1) Command to execute:");
        System.out.println("\t\t'-t' - total wallpaper to command");
        System.out.println("\t\t'-c' - list cubic rooms only");
        System.out.println("\t\t'-d' - list duplicate rooms only");
        System.out.println(" 2) Filepath for the file with list of rooms");
        System.out.println("Ex: -t input1.txt");
    }

}