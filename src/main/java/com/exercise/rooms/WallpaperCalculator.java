package com.exercise.rooms;

import com.exercise.rooms.model.Room;

import java.util.Comparator;
import java.util.stream.Stream;

public class WallpaperCalculator {

    public int wallpaperToCommand(Room room) {
        return room.areaOfSurface() + room.areaOfMinSide();
    }

    public int total(Stream<Room> rooms) {
        return rooms
                .mapToInt(this::wallpaperToCommand)
                .sum();
    }

    public Comparator<Room> byWallpaperNeedComparator() {
        return Comparator.comparingInt(room -> -wallpaperToCommand(room));
    }

}
