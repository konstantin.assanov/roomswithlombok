package com.exercise.rooms;

import com.exercise.rooms.model.Room;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public class RoomAnalyzer {

    public Stream<Room> filterCubicComparingBySurfaceArea(Stream<Room> rooms) {
        return filterCubic(rooms, Comparator.comparingInt(room -> -room.areaOfSurface()));
    }

    public Stream<Room> filterCubic(Stream<Room> rooms, Comparator<Room> byComparator) {
        return rooms
                .filter(Room::isCubic)
                .sorted(byComparator);
    }

    public Stream<Room> filterDuplicate(Stream<Room> rooms) {
        final Set<Room> distinctRooms = new HashSet<>();

        return rooms.filter(room -> !distinctRooms.add(room));
    }

}
