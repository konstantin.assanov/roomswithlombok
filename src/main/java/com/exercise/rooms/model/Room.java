package com.exercise.rooms.model;

import java.util.List;

import lombok.Data;

@Data
public class Room {

    private final int length;
    private final int width;
    private final int height;

    public int areaOfSurface() {
        return 2 * (length * width + width * height + height * length);
    }

    public int areaOfMinSide() {
        return List.of(length * width, width * height, height * length)
                .stream()
                .mapToInt(v -> v)
                .min()
                .orElse(0);
    }

    public boolean isCubic() {
        return (length == width) && (width == height);
    }

}