package com.exercise.rooms.converter;

import com.exercise.rooms.model.Room;

import java.util.Optional;
import java.util.stream.Stream;

public class RoomConverter {

    public static final String DEFAULT_DELIM = "x";

    private final String delimiter;

    public RoomConverter() {
        this(DEFAULT_DELIM);
    }

    public RoomConverter(String delimiter) {
        this.delimiter = delimiter;
    }

    public Optional<Room> toRoom(String roomLine) {

        String[] strSides = roomLine.split(delimiter);

        if (strSides.length < 3)
            return Optional.empty();

        try {
            return Optional.of(
                    new Room(
                            Integer.parseInt(strSides[0]),
                            Integer.parseInt(strSides[1]),
                            Integer.parseInt(strSides[2])
                    ));

        } catch (NumberFormatException nfe) {
            return Optional.empty();
        }

    }

    public String fromRoom(Room room) {
        return "" + room.getLength() + "x" + room.getWidth() + "x" + room.getHeight();
    }

    public Stream<Room> toRooms(Stream<String> rooms) {
        return rooms
                .map(this::toRoom)
                .filter(Optional::isPresent)
                .map(Optional::get);
    }

}