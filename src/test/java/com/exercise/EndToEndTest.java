package com.exercise;

import com.exercise.rooms.RoomAnalyzer;
import com.exercise.rooms.WallpaperCalculator;
import com.exercise.rooms.converter.RoomConverter;
import com.exercise.rooms.model.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EndToEndTest {

    List<String> input = List.of(
            "2x2x2", // 24 + 4 = 28
            "4x2x1", // 16 + 4 + 8 + 2 = 30
            "4x4x4", // 3 * 2 * 4 * 4 + 4 * 4 = 112
            "4x2x1", // 16 + 4 + 8 + 2 = 30
            "4x4x4"); // 3 * 2 * 4 * 4 + 4 * 4 = 112

    RoomConverter converter;

    @BeforeEach
    void init() {
        converter = new RoomConverter();
    }

    @Test
    void shouldCalculateRightTotal() {
        int total = new WallpaperCalculator().total(
            converter.toRooms(input.stream()));

        assertEquals(2 * (112 + 30) + 28, total);
    }

    @Test
    void shouldFindCubicRoomsInDescOrderBySurfaceArea() {
        List<Room> cubicRooms = new RoomAnalyzer()
                .filterCubicComparingBySurfaceArea(
                        converter.toRooms(input.stream()))
                .collect(toList());

        assertEquals(3, cubicRooms.size());

        assertTrue(new Room(4,4,4).equals(cubicRooms.get(0)));
        assertTrue(new Room(4,4,4).equals(cubicRooms.get(1)));
        assertTrue(new Room(2,2,2).equals(cubicRooms.get(2)));
    }

    @Test
    void shouldFindCubicRoomsInDescOrderByWallpaperNeed() {
        List<Room> cubicRooms = new RoomAnalyzer()
                .filterCubic(
                        converter.toRooms(input.stream()),
                        new WallpaperCalculator().byWallpaperNeedComparator())
                .collect(toList());

        assertEquals(3, cubicRooms.size());

        assertTrue(new Room(4,4,4).equals(cubicRooms.get(0)));
        assertTrue(new Room(4,4,4).equals(cubicRooms.get(1)));
        assertTrue(new Room(2,2,2).equals(cubicRooms.get(2)));
    }

    @Test
    void shouldFindDuplicateRooms() {
        List<Room> duplicateRooms = new RoomAnalyzer()
                .filterDuplicate(
                        converter.toRooms(input.stream()))
                .collect(toList());

        assertEquals(2, duplicateRooms.size());

        assertTrue(duplicateRooms.containsAll(List.of(
                new Room(4,2,1),
                new Room(4,4,4)
        )));

    }

}
