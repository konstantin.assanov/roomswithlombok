package com.exercise.rooms.converter;

import com.exercise.rooms.model.Room;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RoomConverterTest {

    RoomConverter converter;

    @BeforeEach
    void init() {
        converter = new RoomConverter();
    }

    @Test
    void shouldConvertSuccessfully() {

        Optional<Room> optRoom = converter.toRoom("3x4x7");

        assertTrue(optRoom.isPresent());

        Room room = optRoom.get();

        assertEquals(new Room(3,4,7), room);
    }

    @Test
    void shouldBeEmptyIfIncomplete() {

        Optional<Room> optRoom = converter.toRoom("3x47");

        assertTrue(optRoom.isEmpty());

    }

    @Test
    void shouldBeEmptyIfInvalidNumbers() {

        Optional<Room> optRoom = converter.toRoom("3x4xA7");

        assertTrue(optRoom.isEmpty());

    }

    @Test
    void shouldConvertStreamOfLinesCorrectly() {

        List<Room> rooms = converter
                .toRooms(List.of("3x4x7", "2x2x1").stream())
                .collect(Collectors.toList());

        assertEquals(2, rooms.size());
        assertTrue(rooms.containsAll(List.of(
                new Room(3,4,7),
                new Room(2,2,1)
        )));
    }

    @Test
    void shouldConvertStreamOfLinesCorrectlyAndSkipInvalidEntries() {

        List<Room> rooms = converter
                .toRooms(List.of("3x4x7", "2x2", "2x2x1", "x3", "23x23x1SA").stream())
                .collect(Collectors.toList());

        assertEquals(2, rooms.size());
        assertTrue(rooms.containsAll(List.of(
                new Room(3,4,7),
                new Room(2,2,1)
        )));
    }

}