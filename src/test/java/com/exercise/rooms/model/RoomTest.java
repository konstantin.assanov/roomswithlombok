package com.exercise.rooms.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RoomTest {

    @Test
    void shouldCalculateAreaOfSurfaceSuccessfully() {

        int area = new Room(1, 2, 3).areaOfSurface();

        assertEquals(22, area);

    }

    @Test
    void shouldCalculateAreaOfMinSideSuccessfully() {

        int minSide = new Room(1, 2, 3).areaOfMinSide();

        assertEquals(2, minSide);

    }

    @Test
    void shouldReturnTrueForCubicRoom() {

        assertTrue(new Room(5, 5, 5).isCubic());

    }

    @Test
    void shouldReturnFalseForNonCubicRoom() {

        assertFalse(new Room(4, 5, 5).isCubic());

    }
}
