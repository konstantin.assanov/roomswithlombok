package com.exercise.rooms;

import com.exercise.rooms.model.Room;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class WallpaperCalculatorTest {

    @Test
    void shouldCalculateWallpaperCorrectly() {

        Room mockRoom = mock(Room.class);

        when(mockRoom.areaOfSurface())
                .thenReturn(22);

        when(mockRoom.areaOfMinSide())
                .thenReturn(2);

        assertEquals(24, new WallpaperCalculator().wallpaperToCommand(mockRoom));
    }

    @Test
    void shouldCalculateRightTotal() {

        WallpaperCalculator calc = new WallpaperCalculator();

        WallpaperCalculator spyCalc = spy(calc);

        List<Room> rooms = List.of(mock(Room.class), mock(Room.class), mock(Room.class));

        doReturn(12).when(spyCalc).wallpaperToCommand(rooms.get(0));
        doReturn(7).when(spyCalc).wallpaperToCommand(rooms.get(1));
        doReturn(6).when(spyCalc).wallpaperToCommand(rooms.get(2));

        assertEquals(25, spyCalc.total(rooms.stream()));
    }

}
