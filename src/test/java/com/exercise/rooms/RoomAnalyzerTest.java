package com.exercise.rooms;

import com.exercise.rooms.model.Room;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

public class RoomAnalyzerTest {

    @Test
    void shouldFindAllCubicRooms() {
        List<Room> rooms = List.of(mock(Room.class), mock(Room.class), mock(Room.class));

        when(rooms.get(0).isCubic()).thenReturn(true);

        when(rooms.get(1).isCubic()).thenReturn(false);

        when(rooms.get(2).isCubic()).thenReturn(true);

        Comparator<Room> comparator = mock(Comparator.class);

        List<Room> cubicRooms = new RoomAnalyzer()
                .filterCubic(rooms.stream(), comparator)
                .collect(Collectors.toList());

        assertEquals(2, cubicRooms.size());

        assertTrue(cubicRooms.containsAll(List.of(rooms.get(0), rooms.get(2))));
    }

    @Test
    void shouldFindAllDuplicateRooms() {
        List<Room> rooms = List.of(
                new Room(1,1,2),
                new Room(2,4,2),
                new Room(1,1,2));

        List<Room> duplicates = new RoomAnalyzer()
                .filterDuplicate(rooms.stream())
                .collect(Collectors.toList());

        assertEquals(1, duplicates.size());

        assertTrue(duplicates.containsAll(List.of(rooms.get(0))));
    }

}
