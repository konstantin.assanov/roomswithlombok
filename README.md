**Wallpaper for Rooms**

# Overview

A renovation company needs to hang new wallpaper in a bunch of rooms. They need to
cover even the floors and ceilings with it.<br>
They run out of wallpaper so they need to submit an order for more. Knowing the dimensions
(length l, width w, and height h) of each room, they want to order only as much as they need.

Every room is a rectangular prism: the surface area of room is 2 * l * w + 2 * w * h + 2 * h * l.

The company also need a little extra wallpaper for each room: the area of the smallest side.

Examples:
 - A room with dimensions 1x2x3 requires 2 * 2 + 2 * 6 + 2 * 3 = 22 square feet of wallpaper
plus additional 2 square feet, for a total of 24 square feet.
 - A room with dimensions 1x1x5 requires 2 * 1 + 2 * 5 + 2 * 5 = 22 square feet of wallpaper
paper plus 1 additional square foot, for a total of 23 square feet.

All numbers in the rooms list are in feet. 

Functionalities:
- calculate number of total square feet of wallpaper the company should order for all
rooms from input (for example, "input1")
- list all rooms from input that have a cubic shape (order by total needed wallpaper
descending)
- list all rooms from input that are appearing more than once (order is irrelevant)

# 1. Run

    java -jar target/rooms-1.0-SNAPSHOT.jar <command> <filepath>

    Following 2 arguments are mandatory:
    1) Command to execute:
		'-t' - total wallpaper to command
		'-c' - list cubic rooms only
		'-d' - list duplicate rooms only
    2) Filepath for the file with list of rooms

    Example: java -jar target/rooms-1.0-SNAPSHOT.jar -t input1.txt

# 2. Comments on implementation

## 2.1. Room

    'Room' describes the room dimensions, and provides 3 method-properties proper to Room:
        1/ Are of whole surface;
        2/ Area of minimal side;
        3/ Is Cubic?

## 2.2. WallpaperCalculator

        1/ Calculate wallpaper needed per room
        2/ Calculate total wallpaper needed for all 'Room' instances from a stream
        3/ Provides a comparator, based on the wallpaper need, to compare the room instances

## 2.3. RoomAnalyzer

        1/ Receives an input stream of 'Room' instances, and
            returns an output stream of cubic 'Room' instances only
            1.1/ Order of output is determined by an injected comparator:
                - Default: area of surface of room descending,
                - Custom: wallpaper needed per room comparator

        2/ Receives an input stream of 'Room' instances, and
            returns an output stream of duplicate 'Room' instances only

## 2.4. RoomConverter

        1/ Convert one string line to a 'Room' instance
        2/ Convert a stream of string lines to a stream of 'Room' instances

## 2.5. AnalyzeRoomsApp

        Provides the console command interface to execute one of 3 functionalities, requiring
        2 arguments: (1) a command to execute, and (2) file path to a file, containing the rooms list.

## Annex

    Main Functionalities are split between the WallpaperCalculator and RoomAnalyzer classes,
    and are not joined in a single class, because Wallper Calculator is about wallpaper management,
    and RoomAnalyzer provides the functionalities related to the 'Room' properties themselves.

    Both RoomAnalyzer and WallpaperCalculator are dependent on Room, but are not dependent
    on each other.

# 3. Tests

    Source code includes unit tests and an 'EndToEndTest' for main functionalities:
        1/ Wallpaper for all rooms to command
        2/ List all cubic rooms in desc order
        3/ List all duplicate rooms
